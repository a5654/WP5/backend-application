package com.bluesoft.accordion.backend.ws.exception;

public class NoResponseException extends RestInvocationException {

    public NoResponseException(String message) {
        super(message);
    }
}
