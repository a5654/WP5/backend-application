package com.bluesoft.accordion.backend.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class RestException extends RuntimeException {

    public RestException(String message) {
        super(message);
    }
}
