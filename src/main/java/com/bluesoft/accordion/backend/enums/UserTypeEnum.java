package com.bluesoft.accordion.backend.enums;

public enum UserTypeEnum {
    APP_PROVIDER,
    MAINTAINER,
    REVIEWER;
}
