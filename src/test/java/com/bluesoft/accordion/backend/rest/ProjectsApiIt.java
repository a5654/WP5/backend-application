package com.bluesoft.accordion.backend.rest;

import com.bluesoft.accordion.backend.repository.ProjectRepository;
import com.bluesoft.accordion.backend.repository.model.ProjectModel;
import com.bluesoft.accordion.backend.rest.util.BaseIt;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class ProjectsApiIt extends BaseIt {
    @Autowired
    MockMvc mvc;

    @Autowired
    ProjectRepository projectRepository;

    @Test
    @Transactional
    public void givenProjects_whenGetProjects_thenStatus200() throws Exception {
        ProjectModel projectModel = new ProjectModel();
        projectModel.setName("Project name");
        projectRepository.save(projectModel);
        mvc.perform(get("/projects").contentType(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
           .andExpect(jsonPath("$.items", hasSize(1)))
           .andExpect(jsonPath("$.items.[0].name", is("Project name")));
    }

}