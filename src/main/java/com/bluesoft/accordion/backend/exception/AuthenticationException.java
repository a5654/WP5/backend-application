package com.bluesoft.accordion.backend.exception;

import lombok.Getter;

@Getter
public class AuthenticationException extends RestException {

  public AuthenticationException(String message) {
      super(message);
  }

}
