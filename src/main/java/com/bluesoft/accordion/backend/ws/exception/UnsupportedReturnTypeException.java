package com.bluesoft.accordion.backend.ws.exception;

public class UnsupportedReturnTypeException extends RestInvocationException {
    public UnsupportedReturnTypeException(String message) {
        super(message);
    }
}
