package com.bluesoft.accordion.backend.ws.annotation;

import com.bluesoft.accordion.backend.ws.DefaultMethodMetadataParser;
import com.bluesoft.accordion.backend.ws.DefaultResponseParser;
import com.bluesoft.accordion.backend.ws.MethodMetadataParser;
import com.bluesoft.accordion.backend.ws.ResponseParser;
import com.bluesoft.accordion.backend.ws.configuration.RestInvokerBeanDefinitionRegistrar;
import org.apache.logging.log4j.util.Strings;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to enable REST invoker services. Will scan the package of the annotated configuration class for
 * service interfaces by default.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({RestInvokerBeanDefinitionRegistrar.class})
@Inherited
public @interface EnableRestInvoker {
    /**
     * Alias for the {@link #value()} attribute.
     */
    String[] value() default {};

    /**
     * Base packages to scan for annotated components. {@link #basePackages()} is an alias for this attribute.
     */
    String[] basePackages() default {};

    /**
     * URL base for all REST service interfaces.
     */
    String baseUrl() default Strings.EMPTY;

    /**
     * Configure the parser class to be used to parse request responses for this particular configuration.
     */
    Class<? extends ResponseParser> responseParser() default DefaultResponseParser.class;

    /**
     * Configure the parser class to be used to parse method metadata for this particular configuration.
     */
    Class<? extends MethodMetadataParser> metadataParser() default DefaultMethodMetadataParser.class;
}
