package com.bluesoft.accordion.backend.repository.model;

import com.bluesoft.accordion.backend.enums.ProjectType;
import com.bluesoft.accordion.backend.enums.ProjectStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "projects")
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode(exclude = "versions")
@ToString(exclude = "versions")
public class ProjectModel {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    private String name;

    private String repositoryName;

    @Column(name= "repository_id")
    private Long repositoryId;

    @Enumerated(STRING)
    private ProjectType type;

    @Enumerated(STRING)
    private ProjectStatus status;

    @CreatedDate
    private LocalDateTime createdAt;

    @CreatedBy
    private String createdBy;

    @LastModifiedDate
    private LocalDateTime modifiedAt;

    @LastModifiedBy
    private String modifiedBy;

    @OneToMany(
            mappedBy = "project",
            fetch = FetchType.LAZY)
    private Set<VersionModel> versions;
}
