package com.bluesoft.accordion.backend.service.mapper;


import com.bluesoft.accordion.backend.repository.model.VersionModel;
import com.bluesoft.accordion.backend.service.model.Version;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface VersionMapper {
    VersionModel toVersionModel(Version version);
    Version toVersion(VersionModel versionModel);
}
