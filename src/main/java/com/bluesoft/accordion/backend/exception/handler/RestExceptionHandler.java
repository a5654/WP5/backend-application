package com.bluesoft.accordion.backend.exception.handler;

import com.bluesoft.accordion.backend.enums.ErrorCodeEnum;
import com.bluesoft.accordion.backend.exception.ApiError;
import com.bluesoft.accordion.backend.exception.AuthenticationException;
import com.bluesoft.accordion.backend.exception.DataNotFoundException;
import com.bluesoft.accordion.backend.exception.FieldErrorWrapper;
import com.bluesoft.accordion.backend.exception.InvalidDataException;
import com.bluesoft.accordion.backend.exception.RestException;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        log.error("Exception caught: ", ex);
        return new ResponseEntity<>(
                 ApiError.of(
                        status,
                        ErrorCodeEnum.INPUT_DATA_IN_INVALID_FORMAT,
                        new FieldErrorWrapper(ex.getBindingResult().getFieldErrors()).getFormattedErrors()
                ),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleApiException(Exception exception) {
        log.error("Exception caught: ", exception);
        return new ResponseEntity<>(
                ApiError.of(HttpStatus.INTERNAL_SERVER_ERROR, ErrorCodeEnum.INTERNAL_SERVER_ERROR),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {
            InvalidDataException.class,
            DataNotFoundException.class,
            AuthenticationException.class
    })
    public ResponseEntity<Object> handleApiException(RestException exception) {
        log.error("Exception caught: ", exception);
        return new ResponseEntity<>(
                ApiError.of(HttpStatus.BAD_REQUEST, exception.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {
            GitLabApiException.class
    })
    public ResponseEntity<Object> handleApiException(GitLabApiException exception) {
        log.error("Exception caught: ", exception);
        return new ResponseEntity<>(
                ApiError.of(HttpStatus.BAD_REQUEST, exception.getMessage()),
                HttpStatus.BAD_REQUEST);
    }
}
