package com.bluesoft.accordion.backend.enums;

public enum ProjectType {
    READY_IMAGES,
    CODE_BASE
}
