package com.bluesoft.accordion.backend.data;

import com.bluesoft.accordion.backend.enums.StatusEnum;
import com.bluesoft.accordion.backend.exception.StatusFlowException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bluesoft.accordion.backend.enums.StatusEnum.BUILDING;
import static com.bluesoft.accordion.backend.enums.StatusEnum.BUILT;
import static com.bluesoft.accordion.backend.enums.StatusEnum.MANUAL_VERIFICATION;
import static com.bluesoft.accordion.backend.enums.StatusEnum.NEW;
import static com.bluesoft.accordion.backend.enums.StatusEnum.READY_FOR_DEPLOYMENT;
import static com.bluesoft.accordion.backend.enums.StatusEnum.REJECTED;
import static com.bluesoft.accordion.backend.enums.StatusEnum.REVIEW;
import static com.bluesoft.accordion.backend.enums.StatusEnum.SUBMITTED;
import static com.bluesoft.accordion.backend.enums.StatusEnum.TESTED;
import static com.bluesoft.accordion.backend.enums.StatusEnum.TESTING;

public class Flow {

    private static Map<StatusEnum, List<StatusEnum>> map = new HashMap<>();

    static {
        map.put(null, List.of(NEW));
        map.put(NEW, List.of(SUBMITTED));
        // todo please confirm REVIEW status
        map.put(SUBMITTED, List.of(REVIEW));
        map.put(REVIEW, List.of(BUILDING));
        map.put(BUILDING, List.of(BUILT, REJECTED));
        map.put(BUILT, List.of(TESTING, REJECTED));
        map.put(TESTING, List.of(TESTED));
        map.put(TESTED, List.of(MANUAL_VERIFICATION));
        map.put(MANUAL_VERIFICATION, List.of(READY_FOR_DEPLOYMENT, REJECTED));
        map.put(READY_FOR_DEPLOYMENT, null);
    }

    public static StatusEnum next(StatusEnum currentStatus) {
        final List<StatusEnum> statusEnums = map.get(currentStatus);
        return statusEnums == null ? currentStatus : statusEnums.get(0);
    }

    public static StatusEnum nextIfPossible(StatusEnum currentStatus, StatusEnum next) {
        final List<StatusEnum> statusEnums = map.get(currentStatus);
        if(statusEnums == null) return currentStatus;
        final int nextIndex = statusEnums.indexOf(next);
        if(nextIndex == -1) {
            throw new StatusFlowException();
        }
        return statusEnums.get(nextIndex);
    }

    public static StatusEnum reject(StatusEnum currentStatus) {
        return nextIfPossible(currentStatus, REJECTED);
    }

}
