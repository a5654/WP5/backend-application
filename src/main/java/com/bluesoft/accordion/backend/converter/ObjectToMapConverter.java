package com.bluesoft.accordion.backend.converter;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class ObjectToMapConverter {

    private final static ObjectMapper mapper = new ObjectMapper();

    public static Map<String, Object> toMap(Object o) {
        return mapper.convertValue(o, Map.class);
    }
}
