package com.bluesoft.accordion.backend.service.model;

import com.bluesoft.accordion.backend.enums.StatusEnum;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class Version implements Comparable<Version> {

    private UUID id;
    private String version;
    private String branchName;
    private StatusEnum status;
    private String lastCommitHash;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime modifiedAt;
    private String modifiedBy;

    @Override
    public int compareTo(Version o) {
        String[] thisVersion = version.split("\\.");
        String[] thatVersion = o.getVersion().split("\\.");
        int length = Math.max(thisVersion.length, thatVersion.length);
        for (int i = 0; i < length; i++) {
            int currentThisPart = i < thisVersion.length ? Integer.parseInt(thisVersion[i]) : 0;
            int currentThatPart = i < thatVersion.length ? Integer.parseInt(thatVersion[i]) : 0;
            if (currentThisPart < currentThatPart) {
                return 1;
            }
            if (currentThisPart > currentThatPart) {
                return -1;
            }
        }
        return 0;
    }


}
