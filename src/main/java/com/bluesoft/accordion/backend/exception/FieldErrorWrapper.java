package com.bluesoft.accordion.backend.exception;

import lombok.AllArgsConstructor;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class FieldErrorWrapper {

    private List<FieldError> errors;

    public List<String> getFormattedErrors() {
        return errors
                .stream()
                .map(er -> String.format("%s : %s", er.getField(), er.getDefaultMessage()))
                .collect(Collectors.toList());
    }

}
