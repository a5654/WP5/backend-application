package com.bluesoft.accordion.backend.enums;

public enum StatusEnum {
    NEW,
    SUBMITTED,
    //todo confirm REVIEW stage
    REVIEW,
    BUILDING,
    BUILT,
    TESTING,
    TESTED,
    MANUAL_VERIFICATION,
    DEPLOYED,
    READY_FOR_DEPLOYMENT,
    REJECTED
}
