package com.bluesoft.accordion.backend.ws.configuration;


import com.bluesoft.accordion.backend.ws.DefaultMethodMetadataParser;
import com.bluesoft.accordion.backend.ws.DefaultResponseParser;
import com.bluesoft.accordion.backend.ws.MethodMetadataParser;
import com.bluesoft.accordion.backend.ws.ResponseParser;
import com.bluesoft.accordion.backend.ws.RestInvokerFactoryBean;
import com.bluesoft.accordion.backend.ws.annotation.EnableRestInvoker;
import com.bluesoft.accordion.backend.ws.annotation.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.data.util.Streamable;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.nonNull;

/**
 * {@link ImportBeanDefinitionRegistrar} to enable {@link EnableRestInvoker} annotation.
 */
@Slf4j
public class RestInvokerBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar, EnvironmentAware, ResourceLoaderAware {

    private static final Class<? extends Annotation> CONFIG_ENABLE_ANNOTATION = EnableRestInvoker.class;
    private static final Class<? extends Annotation> SERVICE_INTERFACE_ANNOTATION = RestService.class;

    private Environment environment;
    private ResourceLoader resourceLoader;


    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
        final AnnotationRestInvokerConfigurationSource configurationSource = new AnnotationRestInvokerConfigurationSource(annotationMetadata, CONFIG_ENABLE_ANNOTATION);
        final Streamable<String> basePackages = configurationSource.getBasePackages();
        Set<BeanDefinition> restServiceCandidates = getBeanDefinitionsAnnotatedBy(basePackages, SERVICE_INTERFACE_ANNOTATION);
        createBeansFor(beanDefinitionRegistry, restServiceCandidates, configurationSource);
    }

    protected void createBeansFor(BeanDefinitionRegistry registry, Set<BeanDefinition> beanDefinitions, AnnotationRestInvokerConfigurationSource configurationSource) {
        String baseUrl = null;
        Optional<String> globalBaseUrl = configurationSource.getBaseUrl();
        if (globalBaseUrl.isPresent()) {
            baseUrl = globalBaseUrl.get();
        }

        Class<? extends ResponseParser> responseParserClass = configurationSource.getResponseParser();
        Class<? extends MethodMetadataParser> metadataParser = configurationSource.getMethodMetadataParser();

        for (BeanDefinition beanDefinition : beanDefinitions) {
            String baseUrlForService = baseUrl;
            String beanName = beanDefinition.getBeanClassName();
            Class<?> serviceClass = ClassUtils.resolveClassName(beanName, resourceLoader.getClassLoader());

            AnnotatedServiceConfigurationSource serviceConfigurationSource = new AnnotatedServiceConfigurationSource(beanDefinition, resourceLoader);
            String localBeanName = serviceConfigurationSource.getBeanName();
            if (StringUtils.hasText(localBeanName)) {
                beanName = localBeanName;
            }

            Class<? extends ResponseParser> localResponseParserClass = serviceConfigurationSource.getResponseParser();
            responseParserClass = getClassOrDefault(localResponseParserClass, responseParserClass, DefaultResponseParser.class);
            String responseParserClassName = responseParserClass != null ? responseParserClass.getName() : null;

            Class<? extends MethodMetadataParser> localMetadataParser = serviceConfigurationSource.getMethodMetadataParser();
            metadataParser = getClassOrDefault(localMetadataParser, metadataParser, DefaultMethodMetadataParser.class);
            String methodMetadataParserClassName = metadataParser != null ? metadataParser.getName() : null;

            Optional<String> baseUrlFromService = serviceConfigurationSource.getBaseUrl();
            if (baseUrlFromService.isPresent()) {
                baseUrlForService = baseUrlFromService.get();
            }

            RootBeanDefinition serviceBeanDefinition = new RootBeanDefinition();
            serviceBeanDefinition.setBeanClass(RestInvokerFactoryBean.class);
            serviceBeanDefinition.setTargetType(serviceClass);

            MutablePropertyValues propertySources = new MutablePropertyValues();
            propertySources.add("serviceClass", serviceClass);
            propertySources.add("baseUrl", baseUrlForService);

            // ADD basic auth authentication config
            Optional<String> basicAuthPrefix = serviceConfigurationSource.getBasicAuthPrefix();
            if (basicAuthPrefix.isPresent()) {
                propertySources.add("basicAuthPrefix", basicAuthPrefix.get());
            } else {
                propertySources.add("basicAuthPrefix", "NONE");
            }

            if (nonNull(responseParserClassName)) {
                propertySources.add("responseParserClassName", responseParserClassName);
            }

            if (nonNull(methodMetadataParserClassName)) {
                propertySources.add("methodMetadataParserClassName", methodMetadataParserClassName);
            }

            serviceBeanDefinition.setPropertyValues(propertySources);

            registry.registerBeanDefinition(beanName, serviceBeanDefinition);
            log.debug("Registered REST service bean with name {}", beanName);
        }
    }

    protected Set<BeanDefinition> getBeanDefinitionsAnnotatedBy(Streamable<String> basePackages, Class<? extends Annotation> annotation) {
        RestServiceInterfaceCandidateProvider candidateProvider = new RestServiceInterfaceCandidateProvider(annotation, environment);
        candidateProvider.setResourceLoader(resourceLoader);

        Set<BeanDefinition> candidates = new HashSet<>();
        for (String basePackage : basePackages) {
            Set<BeanDefinition> beanDefinitions = candidateProvider.findCandidateComponents(basePackage);
            candidates.addAll(beanDefinitions);
        }

        return candidates;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    private <T> Class<? extends T> getClassOrDefault(Class<? extends T> global, Class<? extends T> local, Class<? extends T> defaultClass) {
        if (nonNull(local) && !defaultClass.isAssignableFrom(local)) {
            return local;
        }

        if (nonNull(global) && !defaultClass.isAssignableFrom(global)) {
            return global;
        }

        return null;
    }

}
