package com.bluesoft.accordion.backend.rest.mapper;


import com.bluesoft.accordion.backend.api.model.ProjectDto;
import com.bluesoft.accordion.backend.api.model.ProjectExtendedDto;

import com.bluesoft.accordion.backend.service.model.Project;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface ProjectsApiMapper {
    ProjectExtendedDto toProjectExtendedDto(Project project);
    Project toProject(ProjectDto projectRequestDto);

}
