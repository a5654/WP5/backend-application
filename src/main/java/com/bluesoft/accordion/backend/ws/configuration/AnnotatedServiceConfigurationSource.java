package com.bluesoft.accordion.backend.ws.configuration;


import com.bluesoft.accordion.backend.ws.MethodMetadataParser;
import com.bluesoft.accordion.backend.ws.ResponseParser;
import com.bluesoft.accordion.backend.ws.annotation.RestService;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import java.util.Optional;

public class AnnotatedServiceConfigurationSource {

    private final String serviceClassName;
    private final RestService serviceAnnotation;


    AnnotatedServiceConfigurationSource(BeanDefinition beanDefinition, ResourceLoader resourceLoader) {
        Assert.notNull(resourceLoader, "Resource loader must not be null.");

        serviceClassName = beanDefinition.getBeanClassName();
        Class<?> serviceClass = ClassUtils.resolveClassName(serviceClassName, resourceLoader.getClassLoader());
        serviceAnnotation = AnnotationUtils.findAnnotation(serviceClass, RestService.class);
    }

    String getBeanName() {
        String value = serviceAnnotation.value();
        String beanName = serviceAnnotation.beanName();

        if (value.length() == 0 && beanName.length() == 0) {
            return serviceClassName;
        }

        if (value.length() > 0) {
            return value;
        }

        return StringUtils.hasText(beanName) ? beanName : serviceClassName;
    }

    Optional<String> getBaseUrl() {
        String attribute = serviceAnnotation.baseUrl();
        return StringUtils.hasText(attribute) ? Optional.of(attribute) : Optional.empty();
    }

    Optional<String> getBasicAuthPrefix() {
        String attribute = serviceAnnotation.basicAuthPrefix();
        return StringUtils.hasText(attribute) ? Optional.of(attribute) : Optional.empty();
    }

    Class<? extends ResponseParser> getResponseParser() {
        return serviceAnnotation.responseParser();
    }

    Class<? extends MethodMetadataParser> getMethodMetadataParser() {
        return serviceAnnotation.metadataParser();
    }

}
