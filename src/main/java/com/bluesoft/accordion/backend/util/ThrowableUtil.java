package com.bluesoft.accordion.backend.util;

import org.apache.logging.log4j.util.Strings;

import java.io.PrintWriter;
import java.io.StringWriter;

public final class ThrowableUtil {

    private ThrowableUtil() {
    }

    public static String getStackTrace(Throwable throwable) {
        if (throwable == null) {
            return Strings.EMPTY ;
        }

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        throwable.printStackTrace(pw);
        return sw.toString();
    }

}
