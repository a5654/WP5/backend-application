package com.bluesoft.accordion.backend.rest.mapper;

import com.bluesoft.accordion.backend.api.model.BranchDto;

import org.gitlab4j.api.models.Branch;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.ReportingPolicy.IGNORE;


@Mapper(unmappedTargetPolicy = IGNORE)
public interface BranchApiMapper {

    @Mapping(target = "lastCommitHash", expression = "java(branch.getCommit().getId())")
    BranchDto toBranchDto(Branch branch);


}
