package com.bluesoft.accordion.backend.service;

import com.bluesoft.accordion.backend.util.CookieUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class AuthService {

    @Value("${domain}")
    private String domain;

    private final String GITLAB_COOKIE_NAME = "_gitlab_session";

    public ResponseCookie createGitlabCookie() {

        ResponseCookie deleteCookie = ResponseCookie.from("_gitlab_session","")
                .maxAge(0)
                .path("/")
                .httpOnly(true)
                .domain(domain)
                .build();

        return deleteCookie;
    }
}
