package com.bluesoft.accordion.backend.repository.model;

import com.bluesoft.accordion.backend.enums.UserTypeEnum;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

@Data
@Table(name = "users")
@Entity
@EntityListeners(AuditingEntityListener.class)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    private String name;

    private String username;

    private String email;

    private String avatarUrl;

    //todo what is that field based on?
    @Enumerated
    private UserTypeEnum type;

    private LocalDateTime lastLogin;

    public static UserModel fromMap(Map<String, Object> attributes) {
        return UserModel.builder()
                        .name((String) attributes.get("name"))
                        .username((String) attributes.get("username"))
                        .avatarUrl((String) attributes.get("avatar_url"))
                        .email((String) attributes.get("email"))
                        .lastLogin(LocalDateTime.now())
                        .build();
    }

}
