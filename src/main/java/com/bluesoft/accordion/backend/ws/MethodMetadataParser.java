package com.bluesoft.accordion.backend.ws;

import org.springframework.context.EmbeddedValueResolverAware;

import java.lang.reflect.Method;

/**
 * Interface to be implemented by objects that define a method metadata parser.
 */
public interface MethodMetadataParser extends EmbeddedValueResolverAware {

    /**
     * Parse the method and arguments.
     *
     * @param method method to parse
     * @param args   method arguments
     * @return the method metadata
     * @throws RuntimeException
     */
    RestInvokeMetadata parse(Method method, Object[] args) throws RuntimeException;
}
