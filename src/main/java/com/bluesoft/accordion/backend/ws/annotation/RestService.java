package com.bluesoft.accordion.backend.ws.annotation;

import com.bluesoft.accordion.backend.ws.DefaultMethodMetadataParser;
import com.bluesoft.accordion.backend.ws.DefaultResponseParser;
import com.bluesoft.accordion.backend.ws.MethodMetadataParser;
import com.bluesoft.accordion.backend.ws.ResponseParser;
import org.apache.logging.log4j.util.Strings;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to mark service interfaces.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface RestService {
    /**
     * Alias for bean name
     *
     * @return
     */
    String value() default Strings.EMPTY;

    /**
     * Bean name
     *
     * @return
     */
    String beanName() default Strings.EMPTY;

    /**
     * Base URL used for this service.
     *
     * @return
     */
    String baseUrl() default Strings.EMPTY;

    /**
     * Use basicAuthPrefix prefix.
     *
     * @return
     */
    String basicAuthPrefix() default "NONE";

    /**
     * Configure the parser class to be used to parse request responses for this service.
     */
    Class<? extends ResponseParser> responseParser() default DefaultResponseParser.class;

    /**
     * Configure the parser class to be used to parse method metadata for this service.
     */
    Class<? extends MethodMetadataParser> metadataParser() default DefaultMethodMetadataParser.class;
}
