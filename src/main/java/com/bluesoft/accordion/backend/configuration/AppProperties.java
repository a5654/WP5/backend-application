package com.bluesoft.accordion.backend.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "jwt")
public class AppProperties {

    private final Auth auth = new Auth();
    private final OAuth2 oAuth2 = new OAuth2();

    public static class Auth {
        private String secret;
        private long accessTokenExpirationMins;
        private long refreshTokenExpirationMins;

        public String getSecret() {
            return secret;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        public long getAccessTokenExpirationMins() {
            return accessTokenExpirationMins;
        }

        public long getRefreshTokenExpirationMins() {
            return refreshTokenExpirationMins;
        }

        public void setRefreshTokenExpirationMins(int refreshTokenExpirationMins) {
            this.refreshTokenExpirationMins = refreshTokenExpirationMins;
        }

        public void setAccessTokenExpirationMins(int accessTokenExpirationMins) {
            this.accessTokenExpirationMins = accessTokenExpirationMins;
        }
    }

    public static final class OAuth2 {
        private List<String> authorizedRedirectUris = new ArrayList<>();

        public List<String> getAuthorizedRedirectUris() {
            return authorizedRedirectUris;
        }

        public OAuth2 authorizedRedirectUris(List<String> authorizedRedirectUris) {
            this.authorizedRedirectUris = authorizedRedirectUris;
            return this;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public OAuth2 getOauth2() {
        return oAuth2;
    }

}
