package com.bluesoft.accordion.backend.service;

import com.bluesoft.accordion.backend.api.model.NewVersionStatusDto;
import com.bluesoft.accordion.backend.data.Flow;
import com.bluesoft.accordion.backend.enums.StatusEnum;
import com.bluesoft.accordion.backend.exception.DataNotFoundException;
import com.bluesoft.accordion.backend.exception.InvalidDataException;
import com.bluesoft.accordion.backend.repository.ProjectRepository;
import com.bluesoft.accordion.backend.repository.VersionRepository;
import com.bluesoft.accordion.backend.repository.model.ProjectModel;
import com.bluesoft.accordion.backend.repository.model.VersionModel;
import com.bluesoft.accordion.backend.service.mapper.VersionMapper;
import com.bluesoft.accordion.backend.service.model.Version;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.bluesoft.accordion.backend.util.SessionUtil.getCurrentUsername;

@Service
@RequiredArgsConstructor
public class VersionService {

    private final VersionRepository versionRepository;
    private final ProjectRepository projectRepository;
    private final VersionMapper versionMapper;

    public List<Version> getProjectVersions(UUID projectId) {
        return projectRepository.findByIdAndCreatedBy(projectId, getCurrentUsername())
                                .map(ProjectModel::getVersions)
                                .orElseThrow(DataNotFoundException::new)
                                .stream()
                                .map(versionMapper::toVersion)
                                .sorted()
                                .collect(Collectors.toList());
    }

    //todo should be transactional?
    @Transactional
    public Version addProjectVersion(UUID projectId, Version version) {
        ProjectModel project = projectRepository.findByIdAndCreatedBy(projectId, getCurrentUsername())
                                                          .orElseThrow(InvalidDataException::new);

        final Set<VersionModel> versions = project.getVersions();

        VersionModel versionModel = Optional.ofNullable(version)
                                            .map(versionMapper::toVersionModel)
                                            .orElseThrow(InvalidDataException::new);

        versionModel.setProject(project);

        versionModel.setStatus(Flow.next(version.getStatus()));
        final VersionModel save = versionRepository.save(versionModel);

        return versionMapper.toVersion(save);
    }

    public Optional<Version> getProjectVersion(UUID versionId) {
        return versionRepository.findById(versionId)
                .map(versionMapper::toVersion);
    }

    private VersionModel getStatusVersion(UUID projectId, UUID versionId) {
        ProjectModel project = projectRepository.findById(projectId)
                                                .orElseThrow(DataNotFoundException::new);
        return  project.getVersions()
                                           .stream()
                                           .filter(version -> version.getId().equals(versionId))
                                           .findFirst()
                                           .orElseThrow(DataNotFoundException::new);

    }

    public void updateProjectVersion(UUID projectId, UUID versionId, NewVersionStatusDto statusDto) {
        VersionModel version = getStatusVersion(projectId, versionId);
        if(statusDto == null) {
            version.setStatus(Flow.next(version.getStatus()));
        } else {
            StatusEnum newStatus = StatusEnum.valueOf(statusDto.getStatus());
            version.setStatus(Flow.nextIfPossible(version.getStatus(), newStatus));
        }
        versionRepository.save(version);
    }

    //todo how to do it in a better way?
    public void onSuccess(UUID projectId, UUID versionId) {
        final VersionModel model = getStatusVersion(projectId, versionId);

        model.setStatus(Flow.next(model.getStatus()));
        versionRepository.save(model);
    }
    //todo how to do it in a better way?
    public void onFailure(UUID projectId, UUID versionId) {
        final VersionModel model = getStatusVersion(projectId, versionId);
        model.setStatus(Flow.reject(model.getStatus()));
        versionRepository.save(model);
    }
}
