package com.bluesoft.accordion.backend.service;

import com.bluesoft.accordion.backend.api.model.ProjectExtendedDto;
import com.bluesoft.accordion.backend.enums.StatusEnum;
import com.bluesoft.accordion.backend.exception.InvalidDataException;
import com.bluesoft.accordion.backend.repository.ProjectRepository;
import com.bluesoft.accordion.backend.repository.model.ProjectModel;
import com.bluesoft.accordion.backend.service.mapper.ProjectMapper;
import com.bluesoft.accordion.backend.service.model.Project;
import com.bluesoft.accordion.backend.enums.ProjectStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import com.bluesoft.accordion.backend.exception.DataNotFoundException;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper;

    public List<Project> getProjects(String username) {
        return projectRepository.findAllByCreatedByAndStatus(username, ProjectStatus.ACTIVE)
                                .stream()
                                .map(projectMapper::toProject)
                                .collect(Collectors.toList());
    }

    public Project addProject(Project project) {
        project.setStatus(ProjectStatus.ACTIVE);
        return Optional.ofNullable(project)
                       .map(projectMapper::toProjectModel)
                       .map(projectRepository::save)
                       .map(projectMapper::toProject)
                       .orElseThrow(InvalidDataException::new);
    }

    public Optional<Project> getProject(UUID projectId) {
        return projectRepository.findByIdAndStatus(projectId, ProjectStatus.ACTIVE)
                .map(projectMapper::toProject);
    }

    public void deleteProject(UUID projectId) {
        ProjectModel project = projectRepository.findByIdAndStatus(projectId, ProjectStatus.ACTIVE)
                .orElseThrow(() -> new DataNotFoundException("Project Not Found or is already Deleted"));
        project.setStatus(ProjectStatus.DELETED);
        projectRepository.save(project);
    }


}
