package com.bluesoft.accordion.backend.ws.exception;

public class RestInvocationException extends RuntimeException {

    public RestInvocationException(String message) {
        super(message);
    }
}
