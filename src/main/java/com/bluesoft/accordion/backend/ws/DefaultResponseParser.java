package com.bluesoft.accordion.backend.ws;

import com.bluesoft.accordion.backend.ws.exception.NoResponseException;
import com.bluesoft.accordion.backend.ws.exception.ResponseProcessingException;
import com.bluesoft.accordion.backend.ws.exception.RestInvocationException;
import com.bluesoft.accordion.backend.util.ThrowableUtil;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Response;

import static java.util.Objects.isNull;

@Slf4j
public class DefaultResponseParser implements ResponseParser {

    @Override
    public <T> T parseResponse(Response response, Class<T> clazz) throws RestInvocationException {
        if (isNull(response)) {
            throw new NoResponseException("No response data.");
        }

        if (response.hasEntity() && !(Void.class.equals(clazz) || void.class.equals(clazz))) {
            try {
                return response.readEntity(clazz);
            } catch (ProcessingException e) {
                log.info("REST client response parser error: {}", ThrowableUtil.getStackTrace(e));
                throw new ResponseProcessingException("Bad response data for class " + clazz.getName());
            }
        }

        return null;
    }

}
