package com.bluesoft.accordion.backend.security.oauth2.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;

public abstract class OAuth2UserInfo {

    @JsonIgnore
    protected Map<String, Object> attributes;

    public OAuth2UserInfo(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }
}
