package com.bluesoft.accordion.backend.ws;

import org.springframework.util.StringValueResolver;

public abstract class AbstractMethodMetadataParser implements MethodMetadataParser {

    protected StringValueResolver stringValueResolver;

    @Override
    public void setEmbeddedValueResolver(StringValueResolver stringValueResolver) {
        this.stringValueResolver = stringValueResolver;
    }
}
