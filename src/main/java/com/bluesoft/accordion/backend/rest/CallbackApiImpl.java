package com.bluesoft.accordion.backend.rest;

import com.bluesoft.accordion.backend.api.CallbackApi;
import com.bluesoft.accordion.backend.api.model.AdditionalFailureInfoDto;
import com.bluesoft.accordion.backend.repository.model.LogModel;
import com.bluesoft.accordion.backend.service.LogService;
import com.bluesoft.accordion.backend.service.VersionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
@Slf4j
public class CallbackApiImpl implements CallbackApi {

    private final VersionService versionService;
    private final LogService logService;

    @Override
    public ResponseEntity<Void> onFailure(UUID projectId, UUID versionId, AdditionalFailureInfoDto error) {
        versionService.onFailure(projectId, versionId);
        final LogModel loggedError = logService.logError(versionId, error.getMessage());
        log.error("Error during pipeline: {}", loggedError);
        return ResponseEntity
                .noContent()
                .build();
    }

    @Override
    public ResponseEntity<Void> onSuccess(UUID projectId, UUID versionId) {
        versionService.onSuccess(projectId, versionId);
        return ResponseEntity
                .noContent()
                .build();
    }
}
