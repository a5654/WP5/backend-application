package com.bluesoft.accordion.backend.scheduler;



import com.bluesoft.accordion.backend.service.JenkinsHttpRequestBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@EnableScheduling
@RequiredArgsConstructor
public class JenkinsApiScheduler {

    private final JenkinsHttpRequestBuilder jenkinsHttpRequestBuilder;

    @Scheduled(cron = "${jenkins.trigger.cron}")
    public void triggerTestPipeline(){
        jenkinsHttpRequestBuilder.runTestPipelineForProjectsWithVersionInReadyForDeployment();
    }
}
