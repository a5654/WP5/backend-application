package com.bluesoft.accordion.backend.ws.annotation;

import lombok.Data;

@Data
public class RestInvokerConfig {
    private String baseUrl;
    private String authPrefix;
}
