package com.bluesoft.accordion.backend.ws.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.annotation.Annotation;

@Slf4j
public class RestServiceInterfaceCandidateProvider extends ClassPathScanningCandidateComponentProvider {

    private final Class<? extends Annotation> annClass;

    RestServiceInterfaceCandidateProvider(Class<? extends Annotation> annClass, Environment environment) {
        super(false, environment);
        this.annClass = annClass;
        registerDefaultFilters();
    }

    @Override
    protected boolean isCandidateComponent(AnnotatedBeanDefinition annotatedBeanDefinition) {
        AnnotationMetadata annotationMetadata = annotatedBeanDefinition.getMetadata();
        return annotationMetadata.isIndependent() && annotationMetadata.isInterface();
    }

    @Override
    protected void registerDefaultFilters() {
        this.addIncludeFilter(new AnnotationTypeFilter(annClass, true, true));
    }

}
