package com.bluesoft.accordion.backend.rest.mapper;


import com.bluesoft.accordion.backend.api.model.RepositoryDto;
import org.gitlab4j.api.models.Project;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface RepositoryApiMapper {

    RepositoryDto toRepositoryDto(Project project);
}
