package com.bluesoft.accordion.backend.controller;

import com.bluesoft.accordion.backend.repository.UserRepository;
import com.bluesoft.accordion.backend.repository.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;

    @RequestMapping(
            value = "",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> userInfo(Principal principal) {
        String email = principal.getName();
        UserModel currentUser = userRepository.findByEmail(email).get();
        return ResponseEntity.ok(currentUser);
    }

}
