package com.bluesoft.accordion.backend.service.mapper;

import com.bluesoft.accordion.backend.repository.model.ProjectModel;
import com.bluesoft.accordion.backend.service.model.Project;

import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface ProjectMapper {
    Project toProject(ProjectModel projectModel);
    ProjectModel toProjectModel(Project project);
}
