package com.bluesoft.accordion.backend.repository;

import com.bluesoft.accordion.backend.repository.model.LogModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LogRepository extends JpaRepository<LogModel, UUID> {
}
