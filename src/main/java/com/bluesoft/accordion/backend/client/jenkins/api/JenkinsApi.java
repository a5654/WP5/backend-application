package com.bluesoft.accordion.backend.client.jenkins.api;

import com.bluesoft.accordion.backend.ws.annotation.RestService;

import javax.ws.rs.core.Response;
import javax.ws.rs.*;
import javax.validation.constraints.*;

@Path("/jenkins")
@RestService(baseUrl = "${jenkins.url}",  basicAuthPrefix = "jenkins.client")
public interface JenkinsApi  {
   
    @POST
    @Path("/job/statusFlow/buildWithParameters")
    public Response runTests (@NotNull @QueryParam("REPOSITORY") String repository,
                              @NotNull @QueryParam("BRANCH_NAME") String branchName,
                              @NotNull @QueryParam("ON_SUCCESS") String onSuccess,
                              @NotNull @QueryParam("ON_FAILURE") String onFailure,
                              @NotNull @QueryParam("ON_CHANGE_STATUS") String onChangeStatus);
}
