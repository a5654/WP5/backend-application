package com.bluesoft.accordion.backend.exception;

import com.bluesoft.accordion.backend.enums.ErrorCodeEnum;

import static com.bluesoft.accordion.backend.enums.ErrorCodeEnum.DATA_NOT_FOUND;

public class DataNotFoundException extends RestException {

    private final static ErrorCodeEnum errorMessage = DATA_NOT_FOUND;

    public DataNotFoundException(String message) {
        super(message);
    }

    public DataNotFoundException() {
        this(errorMessage.name());
    }

}
