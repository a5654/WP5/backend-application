package com.bluesoft.accordion.backend;

import com.bluesoft.accordion.backend.configuration.AppProperties;
import com.bluesoft.accordion.backend.ws.annotation.EnableRestInvoker;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableConfigurationProperties({AppProperties.class})
@SpringBootApplication
@EnableJpaAuditing
@ComponentScan(basePackages = {"com.bluesoft"})
@EnableRestInvoker(basePackages = {"com.bluesoft"})
public class AccordionBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccordionBackendApplication.class, args);
    }

}
