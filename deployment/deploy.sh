#!/bin/sh

. ./increment_version.sh --source-only
prod_version=`cat prod_version.txt`
new_version=`increment_version $prod_version 2`
current_path=`pwd`
env_file="docker-compose.env"
target_env="prod"
image_file="accordion.backend.tar.gz"
ssh_address="apps@app.accordion-project.eu"
ssh_port="32022"
echo $new_version > prod_version.txt
echo "VERSION=$new_version" > $env_file
echo "PROFILE=$target_env" >> $env_file


cd ..
echo "[START] BUILDING PROJECT"
mvn clean versions:set -DnewVersion=$new_version package install -DskipTests=true
echo "[FINISHED] BUILDING PROJECT"

echo "[START] BUILDING IMAGE"
docker build -t accordion-backend:$new_version -f Dockerfile-accordion-backend --build-arg target_env=$target_env .
echo "[FINISHED] BUILDING IMAGE"

echo "[START] SAVE IMAGE"
docker save accordion-backend:$new_version | gzip > $image_file
echo "[FINISHED] SAVE IMAGE"

echo "[START] TRANSFER IMAGE"
scp -P $ssh_port $image_file $ssh_address:/home/apps/backend
echo "[FINISHED] TRANSFER IMAGE"

echo "[START] TRANSFER OTHER FILES"
scp -P $ssh_port docker-compose.yaml $ssh_address:/home/apps/backend
scp -P $ssh_port deployment/$env_file $ssh_address:/home/apps/backend
echo "[FINISHED] TRANSFER OTHER FILES"

echo "[START] LOGIN INTO ENV"
ssh $ssh_address -p $ssh_port "cd backend && docker load < $image_file && docker-compose --env-file $env_file up -d"
echo "[FINISHED] LOGIN INTO ENV"
rm $image_file

git add deployment/docker-compose.env deployment/prod_version.txt pom.xml
git commit -m "Deployment into $target_env enviroment with $new_version version "
git push --force

cd $current_path