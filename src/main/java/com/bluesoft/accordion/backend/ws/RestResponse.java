package com.bluesoft.accordion.backend.ws;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestResponse<T> {

    @Getter
    @Setter
    private T result;

    @Setter
    @Getter
    private Throwable throwable;

    @Setter
    @Getter
    private int statusCode;

    @Setter
    @Getter
    private String reasonPhrase;

    /**
     * @return
     */
    public boolean hasResult() {
        return nonNull(result);
    }

    /**
     * @return
     */
    public boolean isCorrect() {
        return isNull(throwable);
    }

    /**
     * Returns <code>true</code> if status code is one of <b>Information response</b> codes.
     *
     * @return <code>true</code> status code is 100 family code,
     * <code>false</code> otherwise
     */
    public boolean isInformation() {
        return statusCode >= 100 && statusCode < 200;
    }

    /**
     * Returns <code>true</code> if status code is one of <b>Successful response</b> codes.
     *
     * @return <code>true</code> status code is 200 family code,
     * <code>false</code> otherwise
     */
    public boolean isSuccessful() {
        return statusCode >= 200 && statusCode < 300;
    }

    /**
     * Returns <code>true</code> if status code is one of <b>Redirection response</b> codes.
     *
     * @return <code>true</code> status code is 300 family code,
     * <code>false</code> otherwise
     */
    public boolean isRedirection() {
        return statusCode >= 300 && statusCode < 400;
    }

    /**
     * Returns <code>true</code> if status code is one of <b>Client error response</b> codes.
     *
     * @return <code>true</code> status code is 400 family code,
     * <code>false</code> otherwise
     */
    public boolean isClientError() {
        return statusCode >= 400 && statusCode < 500;
    }

    /**
     * Returns <code>true</code> if status code is one of <b>Server error response</b> codes.
     *
     * @return <code>true</code> status code is 500 family code,
     * <code>false</code> otherwise
     */
    public boolean isServerError() {
        return statusCode >= 500 && statusCode < 600;
    }

    /**
     * @return
     */
    public String getMessage() {
        if (isNull(throwable)) {
            return Strings.EMPTY;
        }

        return throwable.getMessage();
    }

}
