package com.bluesoft.accordion.backend.enums;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum ErrorCodeEnum {
    INTERNAL_SERVER_ERROR,
    UNAUTHORIZED_USER,
    INSUFFICIENT_PERMISSIONS,
    DATA_NOT_FOUND,
    INPUT_DATA_IN_INVALID_FORMAT,
    CANNOT_GO_TO_THAT_STAGE;
}
