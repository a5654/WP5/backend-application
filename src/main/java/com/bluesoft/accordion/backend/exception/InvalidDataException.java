package com.bluesoft.accordion.backend.exception;

import lombok.Getter;

import static com.bluesoft.accordion.backend.enums.ErrorCodeEnum.INPUT_DATA_IN_INVALID_FORMAT;


@Getter
public class InvalidDataException extends RestException {



    public InvalidDataException(String message) {
        super(message);
    }

    public InvalidDataException() {
        this(INPUT_DATA_IN_INVALID_FORMAT.name());
    }

}
