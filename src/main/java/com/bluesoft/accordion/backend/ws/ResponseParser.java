package com.bluesoft.accordion.backend.ws;

import com.bluesoft.accordion.backend.ws.exception.RestInvocationException;


import javax.ws.rs.core.Response;

/**
 * Interface to be implemented by objects that define a HTTP response parser.
 */
public interface ResponseParser {

    /**
     * Parse the HTTP response.
     *
     * @param response raw HTTP response.
     * @param clazz    body entity class.
     * @param <T>      body entity class.
     * @return the {@link T} object extracted from response.
     * @throws RestInvocationException HTTP status codes wrapper.
     */
    <T> T parseResponse(Response response, Class<T> clazz) throws RestInvocationException;
}
