package com.bluesoft.accordion.backend.ws.configuration;

import com.bluesoft.accordion.backend.ws.MethodMetadataParser;
import com.bluesoft.accordion.backend.ws.ResponseParser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.data.util.Streamable;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Slf4j
public class AnnotationRestInvokerConfigurationSource {

    private static final String BASE_PACKAGES = "basePackages";
    private static final String BASE_URL = "baseUrl";
    private static final String RESPONSE_PARSER = "responseParser";
    private static final String METADATA_PARSER = "metadataParser";

    private final AnnotationMetadata configMetadata;
    private final AnnotationAttributes attributes;


    AnnotationRestInvokerConfigurationSource(AnnotationMetadata metadata, Class<? extends Annotation> annotation) {
        Map<String, Object> annotationAttributes = metadata.getAnnotationAttributes(annotation.getName());
        
        this.attributes = Optional.ofNullable(annotationAttributes)
                                  .map(AnnotationAttributes::new)
                                  .orElseThrow(() -> new IllegalStateException(String.format("Unable to obtain annotation attributes for %s!", annotation)));
        this.configMetadata = metadata;
    }

    Streamable<String> getBasePackages() {
        String[] value = attributes.getStringArray("value");
        String[] basePackages = attributes.getStringArray(BASE_PACKAGES);

        if (value.length == 0 && basePackages.length == 0) {
            String className = configMetadata.getClassName();
            return Streamable.of(ClassUtils.getPackageName(className));
        }

        Set<String> packages = new HashSet<>();
        packages.addAll(Arrays.asList(value));
        packages.addAll(Arrays.asList(basePackages));

        return Streamable.of(packages);
    }

    Optional<String> getBaseUrl() {
        String attribute = attributes.getString(BASE_URL);
        return StringUtils.hasText(attribute) ? Optional.of(attribute) : Optional.empty();
    }

    Class<? extends ResponseParser> getResponseParser() {
        return attributes.getClass(RESPONSE_PARSER);
    }

    Class<? extends MethodMetadataParser> getMethodMetadataParser() {
        return attributes.getClass(METADATA_PARSER);
    }

}
