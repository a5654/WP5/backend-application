package com.bluesoft.accordion.backend.security;

import com.bluesoft.accordion.backend.converter.ObjectToMapConverter;
import com.bluesoft.accordion.backend.repository.model.UserModel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class UserPrincipal implements OAuth2User {

    private String name;
    private String gitlabToken;
    private Collection<? extends GrantedAuthority> authorities;
    private UserModel user;

    public UserPrincipal(String name,
                         String gitlabToken,
                         Collection<? extends GrantedAuthority> authorities,
                         UserModel user) {
        this.name = name;
        this.gitlabToken = gitlabToken;
        this.authorities = authorities;
        this.user = user;
    }

    public static UserPrincipal create(String email, String gitlabToken, UserModel user) {
        List<GrantedAuthority> authorities = Collections.
                singletonList(new SimpleGrantedAuthority("ROLE_USER"));

        return new UserPrincipal(
                email,
                gitlabToken,
                authorities,
                user
        );
    }

    public static UserPrincipal create(String email,  String gitlabToken, Map<String, Object> attributes) {
        UserPrincipal userPrincipal = UserPrincipal.create(email, gitlabToken, UserModel.fromMap(attributes));
        return userPrincipal;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return ObjectToMapConverter.toMap(this.user);
    }

    @Override
    public String getName() {
        return this.name;
    }

    public String getGitlabToken() {
        return gitlabToken;
    }

    public UserModel getGitlabOAuth2UserInfo() {
        return user;
    }
}
