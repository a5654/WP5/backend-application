package com.bluesoft.accordion.backend.repository;

import com.bluesoft.accordion.backend.enums.ProjectStatus;
import com.bluesoft.accordion.backend.repository.model.ProjectModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectModel, UUID> {
    List<ProjectModel> findAllByCreatedByAndStatus(String username, ProjectStatus status);
    Optional<ProjectModel> findByIdAndCreatedBy(UUID uuid, String username);
    Optional<ProjectModel> findByIdAndStatus(UUID uuid, ProjectStatus status);
}
