package com.bluesoft.accordion.backend.repository;

import com.bluesoft.accordion.backend.enums.StatusEnum;
import com.bluesoft.accordion.backend.repository.model.VersionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface VersionRepository extends JpaRepository<VersionModel, UUID> {

    List<VersionModel> findAllByStatus(StatusEnum status);

}
