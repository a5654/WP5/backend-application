java -jar openapi-generator-cli.jar generate -i ../../api/jenkins-api.yaml --api-package com.bluesoft.accordion.backend.client.jenkins.api --model-package com.bluesoft.accordion.backend.client.jenkins.model -g jaxrs-resteasy-eap -DhideGenerationTimestamp=true -o tmp/jenkins
cp -r tmp/jenkins/src/gen/java/com ../../src/main/java/
rm -rf tmp
