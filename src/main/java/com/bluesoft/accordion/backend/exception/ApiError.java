package com.bluesoft.accordion.backend.exception;

import com.bluesoft.accordion.backend.enums.ErrorCodeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiError {

    private int code;
    private String message;
    private List<String> errors;

    private ApiError(HttpStatus httpStatus, String message, List<String> errors) {
        this.code = httpStatus.value();
        this.message = message;
        this.errors = errors;
    }

    public static ApiError of(HttpStatus httpStatus, ErrorCodeEnum message, List<String> errors) {
        return new ApiError(httpStatus, message.name(), errors);
    }
    public static ApiError of(HttpStatus httpStatus, String message, List<String> errors) {
        return new ApiError(httpStatus, message, errors);
    }
    public static ApiError of(HttpStatus httpStatus, String message) {
        return new ApiError(httpStatus, message, null);
    }
    public static ApiError of(HttpStatus httpStatus, ErrorCodeEnum message) {
        return new ApiError(httpStatus, message.name(), null);
    }
}
