package com.bluesoft.accordion.backend.service;


import com.bluesoft.accordion.backend.client.gitlab.GitlabClient;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.CompareResults;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.RepositoryFile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class GitlabService {

    @Getter
    @AllArgsConstructor
    private static class UserTokenAndProject {
        private String token;
        private Integer projectId;
    }


    private final GitlabClient gitlabClient;
    private final ApplicationModelParser applicationModelParser;

    private static final String PROJECTS = "projects";

    private LoadingCache<String, List<Project>> cachedProjects = CacheBuilder.newBuilder()
                                                                             .maximumSize(30L)
                                                                             .expireAfterAccess(5L, TimeUnit.MINUTES)
                                                                             .build(new CacheLoader<String, List<Project>>() {
                                                                                 @Override
                                                                                 public List<Project> load(String s) throws Exception {
                                                                                     List<Project> projects = gitlabClient.getOwnedProjectsFromGitlab(s);
                                                                                     return projects;
                                                                                 }
                                                                             });

    private LoadingCache<UserTokenAndProject, List<Branch>> cachedBranches = CacheBuilder.newBuilder()
                                                                             .maximumSize(100L)
                                                                             .expireAfterAccess(5L, TimeUnit.MINUTES)
                                                                             .build(new CacheLoader<UserTokenAndProject, List<Branch>>() {
                                                                                 @Override
                                                                                 public List<Branch> load(UserTokenAndProject u) throws Exception {
                                                                                     List<Branch> branches = gitlabClient.getBranchesOfProject(u.getToken(), u.getProjectId());
                                                                                     return branches;
                                                                                 }
                                                                             });


    public List<Project> getOwnedProjectsFromGitlab(String userToken) throws GitLabApiException {
        List<Project> projects = cachedProjects.getUnchecked(userToken);
        return projects;
    }

    public List<Branch> getBranchesOfProject(String userToken, Integer projectId) throws GitLabApiException {
        List<Branch> branches = cachedBranches.getUnchecked(new UserTokenAndProject(userToken, projectId));
        return branches;
    }

    public List<String> getImagesFromModel(String userToken, Integer projectId, String branchName) throws GitLabApiException {
        RepositoryFile model = gitlabClient.getModel(userToken, projectId, branchName);
        return applicationModelParser.resolveImageNameFromModel(model.getDecodedContentAsString());
    }

    public CompareResults compareBranches(String userToken, Integer projectId, String from, String to) throws GitLabApiException {
        return gitlabClient.compareBranches(userToken, projectId, from, to);
    }
}
