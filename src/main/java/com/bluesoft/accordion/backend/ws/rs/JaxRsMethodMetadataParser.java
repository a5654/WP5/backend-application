package com.bluesoft.accordion.backend.ws.rs;


import com.bluesoft.accordion.backend.ws.AbstractMethodMetadataParser;
import com.bluesoft.accordion.backend.ws.RestInvokeMetadata;
import com.bluesoft.accordion.backend.ws.annotation.RequestEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.core.annotation.AnnotationUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Slf4j
public class JaxRsMethodMetadataParser extends AbstractMethodMetadataParser {

    @Override
    public RestInvokeMetadata parse(Method method, Object[] args) throws RuntimeException {
        final RestInvokeMetadata metadata = new RestInvokeMetadata();

        String rootPath = Strings.EMPTY;
        Class<?> methodOwnerClass = method.getDeclaringClass();
        final Path rootPathAnnotation = AnnotationUtils.findAnnotation(methodOwnerClass, Path.class);
        if (nonNull(rootPathAnnotation)) {
            rootPath = rootPathAnnotation.value();
        }

        final Path pathAnnotation = AnnotationUtils.findAnnotation(method, Path.class);
        if (isNull(pathAnnotation)) {
            throw new RuntimeException();
        }

        final String path = pathAnnotation.value();

        final List<String> httpMethods = new ArrayList<>();
        if (nonNull(AnnotationUtils.findAnnotation(method, GET.class))) {
            httpMethods.add(HttpMethod.GET);
        }

        if (nonNull(AnnotationUtils.findAnnotation(method, POST.class))) {
            httpMethods.add(HttpMethod.POST);
        }

        if (nonNull(AnnotationUtils.findAnnotation(method, PUT.class))) {
            httpMethods.add(HttpMethod.PUT);
        }

        if (nonNull(AnnotationUtils.findAnnotation(method, DELETE.class))) {
            httpMethods.add(HttpMethod.DELETE);
        }

        if (httpMethods.size() == 0) {
            httpMethods.add(HttpMethod.GET);
        }

        if (httpMethods.size() > 1) {
            throw new IllegalArgumentException("Multiple http method definition for " + method.getName());
        }

        metadata.setMethod(httpMethods.get(0));
        metadata.setPath(rootPath + path);
        metadata.setResponseObjectType(method.getGenericReturnType());

        final Consumes consumes = AnnotationUtils.findAnnotation(method, Consumes.class);
        if (nonNull(consumes)) {
            String[] mediatypes = consumes.value();
            if (mediatypes.length > 0) {
                MediaType[] mediaTypes = Arrays.stream(mediatypes).map(MediaType::valueOf).toArray(MediaType[]::new);
                metadata.setResponseAcceptContentType(mediaTypes);
            }
        }

        final Produces produces = AnnotationUtils.findAnnotation(method, Produces.class);
        if (nonNull(produces)) {
            String[] mediatypes = produces.value();
            if (mediatypes.length > 0) {
                metadata.setRequestContentType(MediaType.valueOf(mediatypes[0]));
            }
        }

        final Annotation[][] parametersAnnotations = method.getParameterAnnotations();
        int idx = 0;
        for (Annotation[] parameterAnnotations : parametersAnnotations) {
            Object parameterValue = args[idx];

            if (parameterValue instanceof String) {
                parameterValue = stringValueResolver.resolveStringValue((String) parameterValue);
            }

            for (Annotation parameterAnnotation : parameterAnnotations) {
                if (PathParam.class.isAssignableFrom(parameterAnnotation.annotationType())) {
                    PathParam pathParam = (PathParam) parameterAnnotation;
                    metadata.addVariableValue(pathParam.value(), parameterValue);
                }

                if (QueryParam.class.isAssignableFrom(parameterAnnotation.annotationType())) {
                    QueryParam queryParam = (QueryParam) parameterAnnotation;
                    String paramName = stringValueResolver.resolveStringValue(queryParam.value());
                    metadata.addQueryParam(paramName, Objects.toString(parameterValue));
                }

                if (CookieParam.class.isAssignableFrom(parameterAnnotation.annotationType())) {
                    CookieParam cookieParam = (CookieParam) parameterAnnotation;
                    String cookieName = stringValueResolver.resolveStringValue(cookieParam.value());
                    metadata.addCookie(cookieName, Objects.toString(parameterValue));
                }

                if (HeaderParam.class.isAssignableFrom(parameterAnnotation.annotationType())) {
                    HeaderParam headerParam = (HeaderParam) parameterAnnotation;
                    String headerName = stringValueResolver.resolveStringValue(headerParam.value());
                    metadata.addHeader(headerName, Objects.toString(parameterValue));
                }

                if (RequestEntity.class.isAssignableFrom(parameterAnnotation.annotationType())) {
                    metadata.setEntity(parameterValue);
                }
            }

            if (parameterAnnotations.length == 0) {
                metadata.setEntity(parameterValue);
            }

            idx++;
        }

        return metadata;
    }

}
