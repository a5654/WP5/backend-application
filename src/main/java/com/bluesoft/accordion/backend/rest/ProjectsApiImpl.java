package com.bluesoft.accordion.backend.rest;

import com.bluesoft.accordion.backend.api.ProjectsApi;
import com.bluesoft.accordion.backend.api.model.BranchDto;
import com.bluesoft.accordion.backend.api.model.BranchListWrapperDto;
import com.bluesoft.accordion.backend.api.model.GetProjectVersionsResponseDto;
import com.bluesoft.accordion.backend.api.model.ImageListWrapperDto;
import com.bluesoft.accordion.backend.api.model.NewVersionStatusDto;
import com.bluesoft.accordion.backend.api.model.ProjectDto;
import com.bluesoft.accordion.backend.api.model.ProjectExtendedDto;
import com.bluesoft.accordion.backend.api.model.ProjectListWrapperDto;
import com.bluesoft.accordion.backend.api.model.RepositoryDto;
import com.bluesoft.accordion.backend.api.model.RepositoryListWrapperDto;
import com.bluesoft.accordion.backend.api.model.VersionDto;
import com.bluesoft.accordion.backend.api.model.VersionExtendedDto;
import com.bluesoft.accordion.backend.enums.StatusEnum;
import com.bluesoft.accordion.backend.exception.InvalidDataException;
import com.bluesoft.accordion.backend.rest.mapper.BranchApiMapper;
import com.bluesoft.accordion.backend.rest.mapper.ProjectsApiMapper;
import com.bluesoft.accordion.backend.rest.mapper.RepositoryApiMapper;
import com.bluesoft.accordion.backend.rest.mapper.VersionApiMapper;
import com.bluesoft.accordion.backend.service.GitlabService;
import com.bluesoft.accordion.backend.service.ProjectService;
import com.bluesoft.accordion.backend.service.VersionService;
import com.bluesoft.accordion.backend.service.model.Version;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.bluesoft.accordion.backend.util.SessionUtil.getCurrentUsername;
import static org.springframework.http.ResponseEntity.notFound;

@RestController
@RequiredArgsConstructor
public class ProjectsApiImpl implements ProjectsApi {

    private final ProjectService projectService;
    private final ProjectsApiMapper projectsApiMapper;
    private final VersionService versionService;
    private final VersionApiMapper versionApiMapper;
    private final RepositoryApiMapper repositoryApiMapper;
    private final BranchApiMapper branchApiMapper;
    private final GitlabService gitlabService;

    @Override
    @SneakyThrows
    public ResponseEntity<RepositoryListWrapperDto> getRepositories(String gitlabToken) {
        RepositoryListWrapperDto result = new RepositoryListWrapperDto();
        result.setRepositories(gitlabService.getOwnedProjectsFromGitlab(gitlabToken)
                                            .stream()
                                            .map(repositoryApiMapper::toRepositoryDto)
                                            .sorted(Comparator.comparing(RepositoryDto::getName))
                                            .collect(Collectors.toList()));

        return ResponseEntity.ok(result);
    }

    @Override
    @SneakyThrows
    public ResponseEntity<BranchListWrapperDto> getRepositoryBranches(Integer id, String gitlabToken) {
        BranchListWrapperDto result = new BranchListWrapperDto();
        result.setBranches(gitlabService.getBranchesOfProject(gitlabToken, id)
                                        .stream()
                                        .map(branchApiMapper::toBranchDto)
                                        .sorted(Comparator.comparing(BranchDto::getName))
                                        .collect(Collectors.toList()));
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<ProjectListWrapperDto> getProjects() {
        List<ProjectExtendedDto> listItemDtos = projectService.getProjects(getCurrentUsername())
                                                              .stream()
                                                              .map(projectsApiMapper::toProjectExtendedDto)
                                                              .sorted(Comparator.comparing(ProjectExtendedDto::getName))
                                                              .collect(Collectors.toList());

        return ResponseEntity.ok(new ProjectListWrapperDto().projects(listItemDtos));
    }

    @Override
    public ResponseEntity<ProjectExtendedDto> addProject(ProjectDto requestBody) {
        return Optional.ofNullable(requestBody)
                       .map(projectsApiMapper::toProject)
                       .map(projectService::addProject)
                       .map(projectsApiMapper::toProjectExtendedDto)
                       .map(ResponseEntity::ok)
                       .orElseThrow(InvalidDataException::new);
    }

    @Override
    public ResponseEntity<VersionExtendedDto> addProjectVersion(UUID projectId, VersionDto requestBody) {
        return Optional.ofNullable(requestBody)
                       .map(versionApiMapper::toVersion)
                       .map(x -> versionService.addProjectVersion(projectId, x))
                       .map(versionApiMapper::toVersionExtendedDto)
                       .map(ResponseEntity::ok)
                       .orElseThrow(InvalidDataException::new);
    }

    @Override
    public ResponseEntity<GetProjectVersionsResponseDto> getProjectVersions(UUID projectId) {
        final List<Version> projectVersions = versionService.getProjectVersions(projectId);
        final List<VersionExtendedDto> projectVersionsLists = projectVersions
                .stream()
                .map(versionApiMapper::toVersionExtendedDto)
                .collect(Collectors.toList());
        final GetProjectVersionsResponseDto responseWrapper = new GetProjectVersionsResponseDto();
        responseWrapper.setVersions(projectVersionsLists);
        return ResponseEntity.ok(responseWrapper);
    }

    @Override
    public ResponseEntity<ProjectExtendedDto> getProject(UUID projectId) {
        return projectService.getProject(projectId)
                             .map(projectsApiMapper::toProjectExtendedDto)
                             .map(ResponseEntity::ok)
                             .orElseThrow(InvalidDataException::new);
    }

    @Override
    public ResponseEntity<VersionExtendedDto> getProjectVersion(UUID versionId) {
        return versionService.getProjectVersion(versionId)
                .map(versionApiMapper::toVersionExtendedDto)
                .map(ResponseEntity::ok)
                .orElseThrow(InvalidDataException::new);
    }
    @Override
    @SneakyThrows
    public ResponseEntity<ImageListWrapperDto> getImagesFromModel(Integer projectId, String branchName, String userToken) {
        List<String> imagesFromModel = gitlabService.getImagesFromModel(userToken, projectId, branchName);
        return Optional.ofNullable(imagesFromModel)
                       .map(images -> new ImageListWrapperDto().items(images))
                       .map(ResponseEntity::ok)
                       .orElseGet(() -> notFound().build());
    }

    @Override
    public ResponseEntity<Void> deleteProject(UUID projectId) {
        projectService.deleteProject(projectId);
        return ResponseEntity.noContent().build();
    }

    @SneakyThrows
    @Override
    public ResponseEntity<Object> compareBranches(Integer id, String xGitlabAuthorization, String from, String to) {
        return ResponseEntity.ok(gitlabService.compareBranches(xGitlabAuthorization, id, from, to));
    }

    @Override
    public ResponseEntity<Void> updateVersionStatus(UUID projectId, UUID versionId, NewVersionStatusDto body) {
        versionService.updateProjectVersion(projectId, versionId, body);
        return ResponseEntity
                            .noContent()
                            .build();
    }
}
