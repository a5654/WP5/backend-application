package com.bluesoft.accordion.backend.ws;

import org.apache.commons.codec.binary.Base64;

import com.bluesoft.accordion.backend.util.ThrowableUtil;
import com.bluesoft.accordion.backend.ws.exception.RestInvocationException;
import com.bluesoft.accordion.backend.ws.exception.UnsupportedReturnTypeException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.core.env.Environment;
import org.springframework.data.util.ReflectionUtils;
import org.springframework.util.StringValueResolver;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;


/**
 * Adapter for Springs {@link FactoryBean} interface to allow easy setup of REST services factories via Spring
 * configuration.
 *
 * @param <T> REST service class
 */
@Slf4j
public class RestInvokerFactoryBean<T> implements FactoryBean<T>, InvocationHandler, EmbeddedValueResolverAware {

    private Client client;
    @Setter
    private String baseUrl;
    @Setter
    private String basicAuthPrefix;
    @Setter
    private Class<?> serviceClass;
    @Setter
    private String methodMetadataParserClassName = DefaultMethodMetadataParser.class.getName();
    private MethodMetadataParser methodMetadataParser;
    private ResponseParser responseParser;
    @Setter
    private String responseParserClassName = DefaultResponseParser.class.getName();
    private StringValueResolver stringValueResolver;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private Environment environment;

    protected RestInvokerFactoryBean() {
        client = ClientBuilder.newClient();
    }

    @PostConstruct
    protected void init() {
        baseUrl = stringValueResolver.resolveStringValue(baseUrl);
        basicAuthPrefix = stringValueResolver.resolveStringValue(basicAuthPrefix);
        if (!"NONE".equals(basicAuthPrefix)) {
            if (Boolean.parseBoolean(environment.getProperty(String.format("%s.enabled", basicAuthPrefix)))) {
                String user = environment.getProperty(String.format("%s.user", basicAuthPrefix));
                String passwordValue = environment.getProperty(String.format("%s.pass", basicAuthPrefix));
                client.register(HttpAuthenticationFeature.basic(user, passwordValue));
            }
        }
    }

    public Object invokeRequest(RestInvokeMetadata metadata) throws RestInvocationException {
        boolean wrapToRestResponse = false;
        Type returnType = metadata.getResponseObjectType();
        Class<?> responseObjectClass;
        if (returnType instanceof Class) {
            responseObjectClass = (Class<?>) returnType;
        } else if (returnType instanceof ParameterizedType) {
            ParameterizedType parameterizedReturnType = (ParameterizedType) returnType;
            Type rawType = parameterizedReturnType.getRawType();
            if (!(rawType instanceof Class && RestResponse.class.isAssignableFrom((Class<?>) rawType))) {
                throw new UnsupportedReturnTypeException("Unsupported return type " + returnType.getTypeName());
            }

            wrapToRestResponse = true;
            Type[] args = parameterizedReturnType.getActualTypeArguments();
            if (args.length != 1) {
                responseObjectClass = Object.class;
            } else {
                Type responseType = args[0];
                if (!(responseType instanceof Class)) {
                    throw new UnsupportedReturnTypeException("Unsupported return type " + returnType.getTypeName() + ", generic type must be class.");
                }
                responseObjectClass = (Class<?>) responseType;
            }
        } else {
            throw new UnsupportedReturnTypeException("Unsupported return type " + returnType.getTypeName());
        }

        Object responseObject;
        Response response = null;
        try {
            Invocation invocation = createInvocation(metadata);
            response = invocation.invoke();

            responseObject = getResponseParser().parseResponse(response, responseObjectClass);

            logResponse(metadata, response, responseObject);

            if (wrapToRestResponse) {
                RestResponse<Object> restResponse = new RestResponse<>();
                restResponse.setResult(responseObject);
                restResponse.setStatusCode(response.getStatusInfo().getStatusCode());
                restResponse.setReasonPhrase(response.getStatusInfo().getReasonPhrase());
                responseObject = restResponse;
            }
        } catch (RuntimeException e) {
            log.error("Response processing error - [{}]: {}\n" +
                            "\tException stack: {}",
                      metadata.getMethod(), metadata.getPath(), ThrowableUtil.getStackTrace(e));

            if (wrapToRestResponse) {
                RestResponse restResponse = new RestResponse();
                restResponse.setThrowable(e);

                if (nonNull(response) && nonNull(response.getStatusInfo())) {
                    restResponse.setStatusCode(response.getStatusInfo().getStatusCode());
                    restResponse.setReasonPhrase(response.getStatusInfo().getReasonPhrase());
                }

                responseObject = restResponse;
            } else {
                throw e;
            }
        }

        return responseObject;
    }

    private void logResponse(RestInvokeMetadata metadata, Response response, Object responseObject) {
        try {
            Object responseBody = getBody(responseObject);
            log.info("[REST Client] Response for [{}]: {}\n" +
                            "\tStatus code: {}\n" +
                            "\tResponse Object: {}\n" +
                            "\tHeaders: {}\n" +
                            "\tCookies: {}\n",
                    metadata.getMethod(), metadata.getPath(), response.getStatus(), responseBody, response.getHeaders(), response.getCookies());
        } catch (IllegalArgumentException e) {
            log.debug("Error occurred while logging response.");
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T getObject() throws Exception {
        return (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{serviceClass}, this);
    }

    @Override
    public Class<?> getObjectType() {
        return serviceClass;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        RestInvokeMetadata metadata = getMethodMetadataParser().parse(method, args);
        return invokeRequest(metadata);
    }

    @Override
    public void setEmbeddedValueResolver(StringValueResolver stringValueResolver) {
        this.stringValueResolver = stringValueResolver;
    }

    protected Invocation createInvocation(RestInvokeMetadata metadata) throws IllegalArgumentException {
        if (isNull(metadata)) {
            throw new IllegalArgumentException("Method metadata can not be null.");
        }

        WebTarget webTarget = client
                .target(baseUrl)
                .path(metadata.getPath())
                .resolveTemplates(metadata.getVariableValues());

        Map<String, String> queryParams = metadata.getQueryParams();
        for (Map.Entry<String, String> param : queryParams.entrySet()) {
            if (!"null".equals(param.getValue())) {
                webTarget = webTarget.queryParam(param.getKey(), param.getValue());
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request();

        Map<String, String> cookies = metadata.getCookies();
        for (Map.Entry<String, String> cookie : cookies.entrySet()) {
            invocationBuilder = invocationBuilder.cookie(cookie.getKey(), cookie.getValue());
        }

        MultivaluedMap<String, Object> headers = metadata.getHeaders();
        for (Map.Entry<String, List<Object>> header : headers.entrySet()) {
            invocationBuilder = invocationBuilder.header(header.getKey(), header.getValue().stream().map(Object::toString).collect(Collectors.joining(", ")));
        }
        invocationBuilder.accept(metadata.getResponseAcceptContentType());


        Object body = getBody(metadata.getEntity());

        log.info("[REST Client] Request created for [{}]: {}\n" +
                        "\tEntity: {}\n" +
                        "\tVariable Values: {}\n" +
                        "\tHeaders: {}\n" +
                        "\tCookies: {}\n" +
                        "\tQuery Params: {}\n" +
                        "\tExpected Response Object Type: {}\n",
                metadata.getMethod(), baseUrl + metadata.getPath(), body, metadata.getVariableValues(), metadata.getHeaders(), metadata.getCookies(), metadata.getQueryParams(), metadata.getResponseObjectType());

        Object entity = metadata.getEntity();
        if (nonNull(entity)) {
            return invocationBuilder.build(metadata.getMethod(), Entity.entity(entity, metadata.getRequestContentType()));
        }

        return invocationBuilder.build(metadata.getMethod());
    }

    private Object getBody(Object entity) {
        try {
           return objectMapper.convertValue(entity, new TypeReference<HashMap<String, Object>>() {});
        } catch (Exception e) {
            return entity.toString();
        }
    }

    protected MethodMetadataParser getMethodMetadataParser() {
        if (isNull(methodMetadataParser)) {
            methodMetadataParser = ReflectionUtils.createInstanceIfPresent(methodMetadataParserClassName, null);
            methodMetadataParser.setEmbeddedValueResolver(stringValueResolver);
        }
        return methodMetadataParser;
    }

    protected ResponseParser getResponseParser() {
        if (isNull(responseParser)) {
            responseParser = ReflectionUtils.createInstanceIfPresent(responseParserClassName, null);
        }

        return responseParser;
    }
}
