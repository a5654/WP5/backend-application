package com.bluesoft.accordion.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
public class ApplicationModelParser {

    private static final String[] YAML_LEVELS = {"topology_template", "node_templates", "Cloud_Framework", "properties", "actions"};
    private static final String PROPERTIES = "properties";
    private static final String IMAGES = "images";
    private static final String INTERNAL = "internal";
    private static final String NAME = "name";

    public List<String> resolveImageNameFromModel(String applicationModel) {
        final Yaml yaml = new Yaml();
        Map<String, Object> actions = getMapFrom(yaml.load(applicationModel), YAML_LEVELS);
        List<String> names = new ArrayList<>();
        for (String actionKey : actions.keySet()) {
            Map<String, Object> action = getMapFrom(actions, actionKey);
            Map<String, Object> properties = getMapFrom(action, PROPERTIES);
            List<Object> images = getList(properties, IMAGES);
            for (Object image : images) {

                for (Object values : getMapValues(image)) {
                    Map k = (Map) values;
                    boolean internal = (boolean) k.get(INTERNAL);
                    if (internal) {
                        Optional.ofNullable(k.get(NAME))
                                .map(String::valueOf)
                                .ifPresent(names::add);
                    }
                }
            }
        }
        return names;
    }

    private Map<String, Object> getMapFrom(Map<String, Object> obj, String... paths) {
        for (String path : paths) {
            obj = (Map) obj.get(path);
        }
        return obj;
    }

    private List<Object> getList(Map<String, Object> obj, String key) {
        return (List) obj.get(key);
    }

    private Collection<Object> getMapValues(Object obj) {
        return ((Map) obj).values();
    }
}
