package com.bluesoft.accordion.backend.rest.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {SecurityConfigTest.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = BaseIt.DockerPostgreDataSourceInitializer.class)
@Testcontainers
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(SpringExtension.class)
@TestInstance(PER_CLASS)
public abstract class BaseIt {

    public static PostgreSQLContainer<?> postgreDBContainer = new PostgreSQLContainer<>("postgres:9.4");

    static {
        postgreDBContainer.start();
    }

    public static class DockerPostgreDataSourceInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        private static final String SPRING_DATASOURCE_PREFIX = "spring.datasource.";
        private static final String EQUAL_TAG = "=";
        private static final String URL_PROPERTY = "url";
        private static final String USERNAME_PROPERTY = "username";
        private static final String PASSWORD_PROPERTY = "password";

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(applicationContext, getProperties());
        }

        private static String[] getProperties() {
            return new String[]{
                    buildProperty(URL_PROPERTY, postgreDBContainer.getJdbcUrl()),
                    buildProperty(USERNAME_PROPERTY, postgreDBContainer.getUsername()),
                    buildProperty(PASSWORD_PROPERTY, postgreDBContainer.getPassword())
            };
        }

        private static String buildProperty(String property, String value) {
            return SPRING_DATASOURCE_PREFIX + property + EQUAL_TAG + value;
        }
    }

    @BeforeEach
    protected void setupContextMocks() {
        SecurityContext securityContext = mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken("username", null);
        when(securityContext.getAuthentication()).thenReturn(authenticationToken);
    }

}