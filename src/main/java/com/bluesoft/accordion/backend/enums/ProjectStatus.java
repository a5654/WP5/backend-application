package com.bluesoft.accordion.backend.enums;

public enum ProjectStatus {
    ACTIVE,
    DELETED
}
