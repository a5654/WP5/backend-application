package com.bluesoft.accordion.backend.service.model;

import com.bluesoft.accordion.backend.enums.DeploymentType;
import com.bluesoft.accordion.backend.enums.ProjectType;
import com.bluesoft.accordion.backend.enums.ProjectStatus;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class Project {
    private UUID id;
    private String name;
    private String repositoryName;
    private Long repositoryId;
    private ProjectType type;
    private ProjectStatus status;
    private DeploymentType deploymentType;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime modifiedAt;
    private String modifiedBy;
}
