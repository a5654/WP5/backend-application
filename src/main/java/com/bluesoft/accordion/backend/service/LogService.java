package com.bluesoft.accordion.backend.service;

import com.bluesoft.accordion.backend.api.model.AdditionalFailureInfoDto;
import com.bluesoft.accordion.backend.repository.LogRepository;
import com.bluesoft.accordion.backend.repository.model.LogModel;
import lombok.RequiredArgsConstructor;
import org.apache.juli.logging.Log;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class LogService {

    private final LogRepository logRepository;


    public LogModel logError(UUID versionId, String description) {

        LogModel newErrorLog = LogModel.builder()
                                       .versionId(versionId)
                                       .description(description)
                                       .build();

        return logRepository.save(newErrorLog);
    }
}
