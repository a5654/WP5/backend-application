package com.bluesoft.accordion.backend.service;

import com.bluesoft.accordion.backend.client.jenkins.api.JenkinsApi;
import com.bluesoft.accordion.backend.enums.StatusEnum;
import com.bluesoft.accordion.backend.repository.VersionRepository;
import com.bluesoft.accordion.backend.repository.model.ProjectModel;
import com.bluesoft.accordion.backend.repository.model.VersionModel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class JenkinsHttpRequestBuilder {

    private final JenkinsApi jenkinsApi;
    private final VersionRepository versionRepository;

    @Value("${backend.url}")
    private String callbackUrl;

    private static final String ON_SUCCESS = "/success/";
    private static final String ON_FAILURE = "/failure/";
    private static final String CHANGE_STATUS = "/projects/%s/versions/%s/status";

    @Transactional
    public void runTestPipelineForProjectsWithVersionInReadyForDeployment() {
        final List<VersionModel> versionsToBuild = versionRepository.findAllByStatus(StatusEnum.SUBMITTED);
        for(VersionModel v : versionsToBuild) {
            ProjectModel p =  v.getProject();
            jenkinsApi.runTests(p.getRepositoryName(),
                                v.getBranchName(),
                                createSuccessFailureCallback(ON_SUCCESS, p.getId(), v.getId()),
                                createSuccessFailureCallback(ON_FAILURE, p.getId(), v.getId()),
                                createChangeStatusCallback(p.getId(), v.getId()));
            //todo confirm REVIEW
            v.setStatus(StatusEnum.REVIEW);
            versionRepository.save(v);
        }
    }

    @SneakyThrows
    private String createSuccessFailureCallback(String callbackType, UUID projectId, UUID versionId) {
        return callbackUrl + callbackType + "/" + projectId + "/" + versionId;
    }

    private String createChangeStatusCallback(UUID projectId, UUID versionId) {
        return String.format(callbackUrl + CHANGE_STATUS, projectId, versionId);
    }
}
