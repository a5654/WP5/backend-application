package com.bluesoft.accordion.backend.exception;

import static com.bluesoft.accordion.backend.enums.ErrorCodeEnum.CANNOT_GO_TO_THAT_STAGE;

public class StatusFlowException extends RestException{

    public StatusFlowException(String message) {
        super(message);
    }

    public StatusFlowException() {
        this(CANNOT_GO_TO_THAT_STAGE.name());
    }
}
