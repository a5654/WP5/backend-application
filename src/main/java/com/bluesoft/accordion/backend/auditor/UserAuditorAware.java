package com.bluesoft.accordion.backend.auditor;

import com.bluesoft.accordion.backend.util.SessionUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserAuditorAware implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable(SessionUtil.getCurrentUsername());
    }
}
