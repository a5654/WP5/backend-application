package com.bluesoft.accordion.backend.ws.exception;

public class ResponseProcessingException extends RestInvocationException {
    public ResponseProcessingException(String message) {
        super(message);
    }
}
