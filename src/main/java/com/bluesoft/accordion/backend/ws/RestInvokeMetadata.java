package com.bluesoft.accordion.backend.ws;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Method metadata used to construct HTTP request.
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RestInvokeMetadata {

    private String path;

    @Builder.Default
    private String method = "GET";

    private Object entity;

    private final Map<String, Object> variableValues = new HashMap<>();
    private final MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
    private final Map<String, String> cookies = new HashMap<>();
    private final Map<String, String> queryParams = new HashMap<>();

    @Builder.Default
    private MediaType requestContentType = MediaType.APPLICATION_JSON_TYPE;
    private Type responseObjectType;

    @Builder.Default
    private MediaType[] responseAcceptContentType = new MediaType[]{MediaType.APPLICATION_JSON_TYPE};


    public void addVariableValue(String name, Object value) {
        variableValues.put(name, value);
    }

    public void addHeader(String name, String... value) {
        headers.put(name, Arrays.asList(value));
    }

    public void addCookie(String name, String value) {
        cookies.put(name, value);
    }

    public void addQueryParam(String name, String value) {
        queryParams.put(name, value);
    }

}
