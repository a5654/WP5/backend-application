package com.bluesoft.accordion.backend.security.oauth2;


import com.bluesoft.accordion.backend.exception.OAuth2AuthenticationProcessingException;
import com.bluesoft.accordion.backend.repository.UserRepository;
import com.bluesoft.accordion.backend.repository.model.UserModel;
import com.bluesoft.accordion.backend.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    private final UserRepository userRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        String gitlabToken = userRequest.getAccessToken().getTokenValue();
        OAuth2User oAuth2User = super.loadUser(userRequest);
        try {
            return processOAuth2User(oAuth2User, gitlabToken);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2User(OAuth2User oAuth2User, String gitlabToken) {
        String email = (String) oAuth2User.getAttributes().get("email");
        if(StringUtils.isEmpty(email)) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }

        UserModel userModel = userRepository.findByEmail(email)
                                            .orElse(UserModel.fromMap(oAuth2User.getAttributes()));

        userModel.setLastLogin(LocalDateTime.now());
        userRepository.save(userModel);

        return UserPrincipal.create(email, gitlabToken, oAuth2User.getAttributes());
    }

}
