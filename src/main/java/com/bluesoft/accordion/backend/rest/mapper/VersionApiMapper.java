package com.bluesoft.accordion.backend.rest.mapper;


import com.bluesoft.accordion.backend.api.model.VersionDto;
import com.bluesoft.accordion.backend.api.model.VersionExtendedDto;
import com.bluesoft.accordion.backend.service.model.Version;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface VersionApiMapper {
    Version toVersion(VersionDto addVersionRequestDto);

    VersionExtendedDto toVersionExtendedDto(Version version);
}
