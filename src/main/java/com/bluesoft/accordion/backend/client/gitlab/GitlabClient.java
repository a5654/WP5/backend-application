package com.bluesoft.accordion.backend.client.gitlab;


import lombok.RequiredArgsConstructor;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.CompareResults;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.RepositoryFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GitlabClient {
    private static final String ACCORDION_MODEL_YAML = "accordion/model.yaml";

    @Value("${gitlab.url}")
    private String gitlabUrl;

    public List<Project> getOwnedProjectsFromGitlab(String userToken) throws GitLabApiException {
        GitLabApi gitLabApi = createGitlabApi(userToken);
        List<Project> projects = gitLabApi.getProjectApi().getOwnedProjects();
        return projects;
    }

    public List<Branch>  getBranchesOfProject(String userToken, Integer projectId) throws GitLabApiException {
        GitLabApi gitLabApi = createGitlabApi(userToken);
        final List<Branch> branches = gitLabApi.getRepositoryApi().getBranches(projectId);
        return branches;
    }

    public CompareResults compareBranches(String userToken, Integer projectId, String from, String to) throws GitLabApiException {
        GitLabApi gitLabApi = createGitlabApi(userToken);
        final CompareResults compare = gitLabApi.getRepositoryApi().compare(projectId, from, to);
        return compare;
    }

    public RepositoryFile getModel(String userToken, Integer projectId, String branchName) throws GitLabApiException {
        GitLabApi gitLabApi = createGitlabApi(userToken);
        return gitLabApi.getRepositoryFileApi().getFile(projectId, ACCORDION_MODEL_YAML, branchName);
    }

    private GitLabApi createGitlabApi(String token) {
        return new GitLabApi(gitlabUrl, Constants.TokenType.OAUTH2_ACCESS, token);
    }
}
