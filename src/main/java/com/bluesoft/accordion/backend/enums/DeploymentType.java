package com.bluesoft.accordion.backend.enums;

public enum DeploymentType {
    AUTO,
    MANUAL
}
